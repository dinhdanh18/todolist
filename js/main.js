import { Item } from "./model/Item.js";
import { todoService } from "./todoService/todoService.js";
import { loadingService } from "./todoService/loadingService.js";

const input = document.getElementById("newTask");
const submit = document.getElementById("addItem");
const todo = document.getElementById("todo");
const aToZ = document.getElementById("two");
const zToA = document.getElementById("three");
const complete = document.getElementById("completed");
let itemList = new Item();
let completeItem = new Item();

const offLoading = () => setTimeout(() => loadingService.offLoading(), 500);

//hiển thị ngày
const pElementDate = document.getElementById("date");
let today = new Date();
let date = today.getDate() + "-" + (today.getMonth() + 1) + "-" + today.getFullYear();
pElementDate.innerHTML = date;


input.addEventListener("keypress", (e) => {
  if (e.key === "Enter") {
    addItem();
  }
});
submit.addEventListener("click", () => {
  addItem();
});

function addItem() {
  loadingService.onLoading();
  let value = input.value;
  let item = {
    value: value,
  };
  if (value.trim() !== "") {
    input.value = "";
    todoService
      .addTodo(item)
      .then((res) => {
        offLoading();
        renderItemService();
      })
      .catch((err) => {
        offLoading();
        alert("error");
      });
  } else {
    offLoading();
    alert("Mời bạn nhập dữ liệu");
  }
}

let deleteItem = (id) => {
  loadingService.onLoading();
  todoService
    .deleteTodo(id)
    .then((res) => {
      offLoading();
      renderItemService();
    })
    .catch((err) => {
      offLoading();
      alert("error");
    });
};
window.deleteItem = deleteItem;
let completeUpdate = (id) => {
  loadingService.onLoading();
  let index = todoService.findIndex(id, itemList);
  itemList[index].complete = true;
  completeItem = itemList[index];
  todoService
    .updateTodo(id, completeItem)
    .then((res) => {
      offLoading();
      renderItem(itemList);
    })
    .catch((err) => {
      offLoading();
      console.log(err);
    });
};
window.completeUpdate = completeUpdate;
function renderItem(list) {
  let content = "";
  let contentComplete = "";
  list.map((item) => {
    if (item.complete === false) {
      content += `
        <li>  <span>${item.value}</span>
        <div class="buttons">
            <button><i class="fas fa-trash remove"onclick="deleteItem(${item.id});")></i></button>
            <button><i class="fas fa-check-circle complete" onclick="completeUpdate(${item.id})"></i></button>
        </div>
        </li>
        `;
    }
    if (item.complete === true) {
      contentComplete += `
        <li>  <span>${item.value}</span>
        <div class="buttons">
            <button><i class="fas fa-trash remove"onclick="deleteItem(${item.id});")></i></button>
            <button><i class="fas fa-check-circle complete"></i></button>
        </div>
        </li>
        `;
    }
    todo.innerHTML = content;
    complete.innerHTML = contentComplete;
  });
}
let renderItemService = () => {
  loadingService.onLoading();
  todoService
    .getTodoList()
    .then((res) => {
      offLoading();
      itemList = res.data;
      renderItem(itemList);
    })
    .catch((err) => {
      offLoading();
      alert(err);
    });
};
renderItemService();

aToZ.addEventListener("click", () => {
  itemList.map((item) => {
    if (item.complete === false) {
      itemList.sort((a, b) => (a.value > b.value ? 1 : -1));
    }
  });
  renderItem(itemList);
});
zToA.addEventListener("click", () => {
  itemList.map((item) => {
    if (item.complete === false) {
      itemList.sort((a, b) => (a.value < b.value ? 1 : -1));
    }
  });
  renderItem(itemList);
});
