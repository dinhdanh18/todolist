export class Item{
    constructor(id,value,complete){
        this.id = id;
        this.value = value;
        this.complete = false;
    }
}