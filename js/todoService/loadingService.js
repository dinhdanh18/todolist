export let loadingService = {
    onLoading: ()=>{
        document.querySelector('#loading-main').style.display = 'flex'
        document.querySelector('.card').style.display = 'none'
    },
    offLoading: ()=>{
        document.querySelector('#loading-main').style.display = 'none'
        document.querySelector('.card').style.display = 'block'
    }
}