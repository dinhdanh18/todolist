

    let BASE_URL = "https://62b0787ae460b79df0469bbc.mockapi.io/todo";
    export let todoService = {
        getTodoList: ()=>{
            return axios({
                url: BASE_URL,
                method: "GET"
            })
        },
        addTodo:(todoList)=>{
            return axios({
                url: BASE_URL,
                method:"POST",
                data: todoList
            })
        },
        deleteTodo:(id)=>{
            return axios({
                url: `${BASE_URL}/${id}`,
                method: "DELETE",
            })
        },
        updateTodo:(id,newList)=>{
            return axios({
                url:`${BASE_URL}/${id}`,
                method:"PUT",
                data:newList,
            })
        },
        findIndex :function(id,list){
            let indexItem = -1 ;
            list.map(function(item,index){
                if(item.id == id){
                    indexItem = index;
                }
            })
            return indexItem;
    }
}
   